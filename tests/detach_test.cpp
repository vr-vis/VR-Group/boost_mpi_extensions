#include "catch.hpp"

#include <array>
#include <iostream>

#include <boost_mpi_extensions/mpi_detach.hpp>

void detach_callback       (void* data)
{
  std::cout << "detach_callback: "        << static_cast<char*>(data) << std::endl;
}
void detach_status_callback(void* data,                     MPI_Status* status)
{
  std::cout << "detach_stati_callback: "  << static_cast<char*>(data) << " Tag: " << status->MPI_TAG << "\n";
}
void detach_stati_callback (void* data, std::int32_t count, MPI_Status* status)
{
  std::cout << "detach_stati_callback: "  << static_cast<char*>(data) << " Tags: ";
  for (auto i = 0; i < count; i++)
    std::cout << status[i].MPI_TAG << " ";
  std::cout << "\n";
}

TEST_CASE("MPI_Detach", "MPI_Detach")
{
  auto argc  = 1;
  auto argv  = std::vector<char*>{"detach_test"};
  auto argvp = argv.data();
  MPI_Init(&argc, &argvp);

  {
    auto source  = 0;
    auto target  = 0;
    auto request = MPI_Request();
    MPI_Isend (&source , 1, MPI_INT, 0, 23, MPI_COMM_SELF, &request);
    MPI_Detach(&request, detach_callback, "Sent data with MPI_Isend.");
    MPI_Recv  (&target , 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }
  {
    auto source  = 0;
    auto target  = 0;
    auto request = MPI_Request();
    MPI_Isend (&source , 1, MPI_INT, 0, 23, MPI_COMM_SELF, &request);
    MPI_Detach_status(&request, detach_status_callback, "Sent data with MPI_Isend.");
    MPI_Recv  (&target , 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }
  {
    auto sources  = std::array<int        , 10> {};
    auto targets  = std::array<int        , 10> {};
    auto requests = std::array<MPI_Request, 10> {};
    for (auto i = 0; i < 10; ++i)
      MPI_Isend(&sources[i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, &requests[i]);
    MPI_Detach_all(10, requests.data(), detach_callback, "Sent 10 data with MPI_Isend.");
    for (auto i = 0; i < 10; ++i)
      MPI_Recv(&targets [i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }
  {
    auto sources  = std::array<int        , 10> {};
    auto targets  = std::array<int        , 10> {};
    auto requests = std::array<MPI_Request, 10> {};
    for (auto i = 0; i < 10; ++i)
      MPI_Isend(&sources[i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, &requests[i]);
    MPI_Detach_all_status(10, requests.data(), detach_stati_callback, "Sent 10 data with MPI_Isend.");
    for (auto i = 0; i < 10; ++i)
      MPI_Recv (&targets[i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }
  {
    auto sources       = std::array<int        , 10> {};
    auto targets       = std::array<int        , 10> {};
    auto requests      = std::array<MPI_Request, 10> {};
    const char* data[] =
    {
      "Sent data 1  with MPI_Isend.",
      "Sent data 2  with MPI_Isend.",
      "Sent data 3  with MPI_Isend.",
      "Sent data 4  with MPI_Isend.",
      "Sent data 5  with MPI_Isend.",
      "Sent data 6  with MPI_Isend.",
      "Sent data 7  with MPI_Isend.",
      "Sent data 8  with MPI_Isend.",
      "Sent data 9  with MPI_Isend.",
      "Sent data 10 with MPI_Isend."
    };
    
    for (auto i = 0; i < 10; ++i)
      MPI_Isend(&sources[i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, &requests[i]);
    MPI_Detach_each(10, requests.data(), detach_callback, reinterpret_cast<void**>(&data));
    for (auto i = 0; i < 10; ++i)
      MPI_Recv (&targets [i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }
  {
    auto sources       = std::array<int        , 10> {};
    auto targets       = std::array<int        , 10> {};
    auto requests      = std::array<MPI_Request, 10> {};
    const char* data[] =
    {
      "Sent data 1  with MPI_Isend.",
      "Sent data 2  with MPI_Isend.",
      "Sent data 3  with MPI_Isend.",
      "Sent data 4  with MPI_Isend.",
      "Sent data 5  with MPI_Isend.",
      "Sent data 6  with MPI_Isend.",
      "Sent data 7  with MPI_Isend.",
      "Sent data 8  with MPI_Isend.",
      "Sent data 9  with MPI_Isend.",
      "Sent data 10 with MPI_Isend."
    };
    
    for (auto i = 0; i < 10; ++i)
      MPI_Isend(&sources[i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, &requests[i]);
    MPI_Detach_each_status(10, requests.data(), detach_status_callback, reinterpret_cast<void**>(&data));
    for (auto i = 0; i < 10; ++i)
      MPI_Recv (&targets [i], 1, MPI_INT, 0, 23, MPI_COMM_SELF, MPI_STATUS_IGNORE);
  }

  mpi::detach::MPI_Finalize();
}
