#ifndef MPI_DETACH_HPP
#define MPI_DETACH_HPP

#define BOOST_THREAD_VERSION 5

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <list>
#include <mutex>
#include <optional>
#include <thread>
#include <utility>
#include <variant>
#include <vector>

#include <mpi.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/beast/core/span.hpp>
#include <boost/mpi.hpp>

namespace mpi::detach
{
namespace detail
{
using namespace std::chrono_literals;

struct request
{
  // Note: The void* is outdated practice, yet the alternative in this case requires std::any in state or the reflection proposal.
  using callback_variant = std::variant<std::function<void(void*)>, std::function<void(void*, MPI_Status*)>>;

  request           (const MPI_Request& request, callback_variant callback, void* user_data = nullptr)
  : native   (request)
  , callback (std::move(callback))
  , user_data(user_data)
  {

  }
  request           (const request&  that) = default;
  request           (      request&& temp) = default;
 ~request           ()                     = default;
  request& operator=(const request&  that) = default;
  request& operator=(      request&& temp) = default;

  [[nodiscard]]
  bool ignores_status() const
  {
    return std::holds_alternative<std::function<void(void*)>>(callback);
  }

  MPI_Request      native;
  callback_variant callback;
  void*            user_data;
  MPI_Status       status {};
};
struct collective_request
{
  // Note: The void* is outdated practice, yet the alternative in this case requires std::any in state or the reflection proposal.
  using callback_variant = std::variant<std::function<void(void*)>, std::function<void(void*, std::int32_t, MPI_Status*)>>;

  collective_request           (std::vector<MPI_Request> requests, callback_variant callback, void* user_data = nullptr) 
  : native   (std::move(requests))
  , callback (std::move(callback))
  , user_data(user_data)
  {
    if (!ignores_status()) 
      stati.resize(native.size()); // Bug prone: Resizing native post-constructor leads to inconsistent stati size.
  }
  collective_request           (const collective_request&  that) = default;
  collective_request           (      collective_request&& temp) = default;
 ~collective_request           ()                                = default;
  collective_request& operator=(const collective_request&  that) = default;
  collective_request& operator=(      collective_request&& temp) = default;

  [[nodiscard]]
  bool ignores_status() const
  {
    return std::holds_alternative<std::function<void(void*)>>(callback);
  }

  std::vector<MPI_Request> native;
  callback_variant         callback;
  void*                    user_data;
  std::vector<MPI_Status>  stati {};
};
struct state
{
  state           () : detach_thread([this] { run(); }), detach_thread_running(true)
  {

  }
  state           (const state&  that) = delete;
  state           (      state&& temp) = delete;
 ~state           ()
  {
    {
      std::unique_lock<std::mutex> lock(container_mutex);
      while (!requests.empty() || !collective_requests.empty())
      {
        container_condition_variable.notify_one();
        lock.unlock();
        std::this_thread::sleep_for(2ms);
        lock.lock  ();
      }
      detach_thread_running = false;
      container_condition_variable.notify_one();
    }
    detach_thread.join();
  }
  state& operator=(const state&  that) = delete;
  state& operator=(      state&& temp) = delete;

  void run()
  {
    while (detach_thread_running || !active_requests.empty() || !active_collective_requests.empty()) 
    {
      do 
      {
        std::unique_lock<std::mutex> lock(container_mutex);
        if (!requests           .empty()) active_requests           .splice(active_requests           .begin(), requests);
        if (!collective_requests.empty()) active_collective_requests.splice(active_collective_requests.begin(), collective_requests);

        if (active_requests.empty() && active_collective_requests.empty())
          while (detach_thread_running && requests.empty() && collective_requests.empty())
            container_condition_variable.wait(lock);
      }
      while (detach_thread_running && active_requests.empty() && active_collective_requests.empty());

      if (!active_requests           .empty()) 
      {
        auto current = active_requests.begin();
        auto end     = active_requests.end  ();
        while (current != end) 
        {
          auto done {0};

          MPI_Test(&current->native, &done, current->ignores_status() ? MPI_STATUS_IGNORE : &current->status);
          if (done) 
          {
            current->ignores_status()
              ? std::get<std::function<void(void*)>>             (current->callback)(current->user_data)
              : std::get<std::function<void(void*, MPI_Status*)>>(current->callback)(current->user_data, &current->status);
            current = active_requests.erase(current);
          }
          else
            ++current;
        }
      }
      if (!active_collective_requests.empty()) 
      {
        auto current = active_collective_requests.begin();
        auto end     = active_collective_requests.end  ();
        while (current != end) 
        {
          auto done {0};

          MPI_Testall(static_cast<std::int32_t>(current->native.size()), current->native.data(), &done, current->ignores_status() ? MPI_STATUS_IGNORE : current->stati.data());
          if (done) 
          {
            current->ignores_status()
              ? std::get<std::function<void(void*)>>                           (current->callback)(current->user_data)
              : std::get<std::function<void(void*, std::int32_t, MPI_Status*)>>(current->callback)(current->user_data, static_cast<std::int32_t>(current->native.size()), current->stati.data());
            current = active_collective_requests.erase(current);
          }
          else
            ++current;
        }
      }

      std::this_thread::sleep_for(2ms);
    }
  }
                                    
  std::thread                   detach_thread;
  std::atomic_bool              detach_thread_running;

  std::mutex                    container_mutex;
  std::condition_variable       container_condition_variable;

  std::list<request>            requests                   {};
  std::list<collective_request> collective_requests        {};
  std::list<request>            active_requests            {};
  std::list<collective_request> active_collective_requests {};
};

inline std::optional<state> global_state; // Note: External-linkage optional used as a lazy-initialized stack variable. Must be reset prior to MPI_Finalize.

class detach_service
{
public:
  explicit detach_service  (const std::size_t thread_count = 1)
  : work_guard_(boost::asio::make_work_guard(io_context_))
  {
    for (auto i = 0; i < thread_count; ++i)
      threads_.create_thread([&] () { io_context_.run(); });
  }
  detach_service           (const detach_service&  that) = delete ;
  detach_service           (      detach_service&& temp) = delete ;
 ~detach_service           ()                            = default;
  detach_service& operator=(const detach_service&  that) = delete ;
  detach_service& operator=(      detach_service&& temp) = delete ;

  void post_request           ()
  {
    
  }
  void post_collective_request()
  {
    
  }

protected:
  boost::thread_group                                                      threads_   ;
  boost::asio::io_context                                                  io_context_;
  boost::asio::executor_work_guard<boost::asio::io_context::executor_type> work_guard_;
                                                               
  std::mutex                                                               container_mutex_;
  std::condition_variable                                                  container_condition_variable_;

  std::list<request>                                                       requests_                   {};
  std::list<collective_request>                                            collective_requests_        {};
  std::list<request>                                                       active_requests_            {};
  std::list<collective_request>                                            active_collective_requests_ {};
};
}

typedef void MPI_Detach_callback         (void*);
typedef void MPI_Detach_callback_status  (void*,               MPI_Status*);
typedef void MPI_Detach_callback_statuses(void*, std::int32_t, MPI_Status*);

// Note: If the test does not succeed immediately, takes the ownership of the request and invalidates it.
inline std::int32_t MPI_Detach            (                    MPI_Request* request , MPI_Detach_callback*          callback, void*  data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done {0};

  MPI_Test(request, &done, MPI_STATUS_IGNORE);
  if (done) 
    callback(data);
  else 
  {
    std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
    detail::global_state->requests.emplace_back(*request, callback, data);
    detail::global_state->container_condition_variable.notify_one();
    *request = MPI_REQUEST_NULL;
  }

  return MPI_SUCCESS;
}
// Note: If the test does not succeed immediately, takes the ownership of the request and invalidates it.                                   
inline std::int32_t MPI_Detach_status     (                    MPI_Request* request , MPI_Detach_callback_status*   callback, void*  data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done   {0};
  auto status {MPI_Status()};

  MPI_Test(request, &done, &status);
  if (done)
    callback(data, &status);
  else
  {
    std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
    detail::global_state->requests.emplace_back(*request, callback, data);
    detail::global_state->container_condition_variable.notify_one();
    *request = MPI_REQUEST_NULL;
  }

  return MPI_SUCCESS;
}
// Note: If the test does not succeed immediately, takes the ownership of the requests and invalidates them.                                
inline std::int32_t MPI_Detach_each       (std::int32_t count, MPI_Request* requests, MPI_Detach_callback*          callback, void** data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done {0};

  for (auto i = 0; i < count; ++i) 
  {
    MPI_Test(&requests[i], &done, MPI_STATUS_IGNORE);
    if (done)
      callback(data[i]);
    else 
    {
      std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
      detail::global_state->requests.emplace_back(requests[i], callback, data[i]);
      detail::global_state->container_condition_variable.notify_one();
      requests[i] = MPI_REQUEST_NULL;
    }
  }

  return MPI_SUCCESS;
}
// Note: If the test does not succeed immediately, takes the ownership of the requests and invalidates them.                                
inline std::int32_t MPI_Detach_each_status(std::int32_t count, MPI_Request* requests, MPI_Detach_callback_status*   callback, void** data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done   {0};
  auto status {MPI_Status()};

  for (auto i = 0; i < count; ++i) 
  {
    MPI_Test(&requests[i], &done, &status);
    if (done)
      callback(data[i], &status);
    else 
    {
      std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
      detail::global_state->requests.emplace_back(requests[i], callback, data[i]);
      detail::global_state->container_condition_variable.notify_one();
      requests[i] = MPI_REQUEST_NULL;
    }
  }

  return MPI_SUCCESS;
}
// Note: If the test does not succeed immediately, takes the ownership of the requests and invalidates them.                                
inline std::int32_t MPI_Detach_all        (std::int32_t count, MPI_Request* requests, MPI_Detach_callback*          callback, void*  data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done {0};

  MPI_Testall(count, requests, &done, MPI_STATUSES_IGNORE);
  if (done)
    callback(data);
  else 
  {
    std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
    detail::global_state->collective_requests.emplace_back(std::vector<MPI_Request>(requests, requests + count), callback, data);
    detail::global_state->container_condition_variable.notify_one();
    for (auto i = 0; i < count; ++i)
      requests[i] = MPI_REQUEST_NULL;
  }

  return MPI_SUCCESS;
}
// Note: If the test does not succeed immediately, takes the ownership of the requests and invalidates them.                                
inline std::int32_t MPI_Detach_all_status (std::int32_t count, MPI_Request* requests, MPI_Detach_callback_statuses* callback, void*  data)
{
  if (!detail::global_state) detail::global_state.emplace();

  auto done  {0};
  auto stati {std::vector<MPI_Status>(count)};

  MPI_Testall(count, requests, &done, stati.data());
  if (done)
    callback(data, count, stati.data());
  else
  {
    std::unique_lock<std::mutex> lock(detail::global_state->container_mutex);
    detail::global_state->collective_requests.emplace_back(std::vector<MPI_Request>(requests, requests + count), callback, data);
    detail::global_state->container_condition_variable.notify_one();
    for (auto i = 0; i < count; ++i)
      requests[i] = MPI_REQUEST_NULL;
  }

  return MPI_SUCCESS;
}
inline std::int32_t MPI_Finalize          ()
{
  detail::global_state.reset();
  return PMPI_Finalize();
}
}
using namespace mpi::detach; // Note: Move to global namespace to match the MPI_ definitions.

#endif